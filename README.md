# mysql-operator

This service is under heavy development.

## Installation

Install with 

```sh
helmfile apply
```

## Usage

You have to set the orchestrators topology password when using this chart!

```
orchestrator:
  topologyPassword: CHANGEME
```
